// logIn Function

function logIn(){
	let username = prompt("Enter your username:").toLowerCase();
	let password = prompt("Enter your password:").toLowerCase();
	let role = prompt("Enter your role:").toLowerCase();
	if(username === "" || password === "" || role === ""){
		alert("Input should not be empty");
	}
	else{
		switch(role){
			case 'admin':
				alert("Welcome back to the class portal, admin!");
				break;
			case 'teacher':
				alert("Thank you for logging in, teacher!");
				break;
			case 'rookie':
				alert("Welcome to the class portal, student!");
				break;
			default:
				alert("Role out of range.");
				break;
		}
	}
}
logIn();

// average Function

function checkAverage(num1, num2, num3, num4){
	let avg = (num1 + num2 + num3 + num4)/4;
	avg = Math.round(avg);
	if(avg <= 74){
		console.log("Hello, student, your average is: " + avg + " . The letter equivalent is F");
	}
	else if(avg >= 75 && avg <= 79){
		console.log("Hello, student, your average is: " + avg + " . The letter equivalent is D");
	}
	else if(avg >= 79 && avg <= 84){
		console.log("Hello, student, your average is: " + avg + " . The letter equivalent is C");
	}
	else if(avg >= 85 && avg <= 89){
		console.log("Hello, student, your average is: " + avg + " . The letter equivalent is B");
	}
	else if(avg >= 90 && avg <= 95){
		console.log("Hello, student, your average is: " + avg + " . The letter equivalent is A");
	}
	else if(avg >= 96){
		console.log("Hello, student, your average is: " + avg + " . The letter equivalent is A+");
	}
	
}
